import psycopg2
import sys


def crearTaula(conn):
    try:
        cur = conn.cursor()
        cur.execute("CREATE TABLE Clients(id_client INT PRIMARY KEY, nom VARCHAR, cognom VARCHAR, telefon INT, correu VARCHAR, adreca VARCHAR, ciutat VARCHAR);")

        conn.commit()
    except (Exception, psycopg2.DatabaseError) as error:
        if conn is not None:
            conn.rollback()
            print("--Error {0}".format(error))
            sys.exit(1)
