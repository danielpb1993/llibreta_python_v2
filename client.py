class Client:
    def __init__(self, id_client, nom, cognom, telefon, correu, adreca, ciutat):
        self.id_client = id_client
        self.nom = nom
        self.cognom = cognom
        self.telefon = telefon
        self.correu = correu
        self.adreca = adreca
        self.ciutat = ciutat

    def __str__(self):
        return "Client ID: {}\nNom: {}\nCognom: {}\nTelefon: {}\nCorreu: {}\nAdreça: {}\nCiutat: {}\n".format(
            self.id_client, self.nom, self.cognom, self.telefon, self.correu, self.adreca, self.ciutat)

